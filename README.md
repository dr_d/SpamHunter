# Spam Hunter #

## Overview ##

Spam Hunter is an open source Google Apps Script that allows you to identify and delete unwanted spam.  In order to use it, you will have to authorize this script to access your Gmail-->Spam folder.

The GAS web app can do three things:
1.  Identify the source of spam from Gmail and all accounts that are attached via IMAP and POP3.
2.  Track spammers by creating a spreadsheet that lists all spammer's addresses and 'reply to' email addresses. It also runs automatically and helps you track spam mail sources over time.
3.  Automatically move spam from the spam folder to the Trash to free up room on Google Drive.

To give the app a try, please visit [Spam Hunter](https://goo.gl/oIerKi).
