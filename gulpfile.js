//spamhunter gulp file


var gulp = require('gulp');
var sass = require('gulp-sass');
var nano = require('gulp-cssnano');
var rename = require('gulp-rename');
var watch = require('gulp-watch');


//compile custom scss
gulp.task('sass', function () {
    gulp.src('scss/*.scss')
        .pipe(sass())
        .pipe(rename({basename:'styles'}))
        .pipe(gulp.dest('./css/'));
});

//minify css
gulp.task('nano', function() {
  return gulp.src('css/*.css')
    .pipe(nano())
    .pipe(rename({extname: '.min.css'}))
    .pipe(gulp.dest('./css'));
});

//watch scss folders during dev
gulp.task('automate', function() {
    gulp.watch('scss/*', ['sass']);
});


//default task
gulp.task('default', ['sass', 'nano']);
