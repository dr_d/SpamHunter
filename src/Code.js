/* USER INTERFACE & HTML GETTERS & SETTERS
    This web app includes a user interface to make running the functions easier.
    First, the HMTL Service is called to get the HTML.
    The HTML includes simple user events to run the functions in the script.
   Shortened URL: https://goo.gl/oIerKi
*/
function doGet() {
  var index =  HtmlService.createTemplateFromFile('index')
      .evaluate()
      .setSandboxMode(HtmlService.SandboxMode.IFRAME);
      index.setTitle("Spam Hunter");
      return index;
}
function include(data) {
        return HtmlService.createHtmlOutputFromFile(data)
        .getContent();
}

/* FIRSTRUN Create a spreadsheet for the log
      The code below creates a new spreadsheet "Spam Hunter" with 501 rows and 4 columns and logs the
      URL for it */
function firstRun(){
     var newSheet = SpreadsheetApp.create("Spam Hunter", 501, 4);
     Logger.log(newSheet.getUrl());
     var url = newSheet.getUrl();
     var id = newSheet.getId();
     var sheet = newSheet.getSheets()[0];
// Appends a new row with 4 columns at the top of the first sheet
     sheet.appendRow(["Date", "Spammer", "Reply Address", "Recipient"]);
// Freezes the first row
     sheet.setFrozenRows(1);
//format the first row & make the columns pretty
  var range = sheet.getRange("A1:D1");
  range.setBackground("#808080");
  range.setFontSize(14);
  range.setFontFamily("Helvetica");
  range.setFontColor("white");
  sheet.autoResizeColumn(1);
  sheet.autoResizeColumn(2);
  sheet.autoResizeColumn(3);
  sheet.autoResizeColumn(4);
};

/*FIND THE SPREADSHEET FOR ADD SPAMMERS FUNCTION BELOW
  After firstRun creates the sheet this code runs globally to open the Spam Hunter Spreadsheet and log spam */
    var sheets = DriveApp.getFilesByName("Spam Hunter");
      var oneTime=0;
         while (sheets.hasNext() & (oneTime < 1)) {
           var target = sheets.next();
           var id = target.getId();  //get the ID
           var name = target.getName(); //get the filename
           Logger.log(id);
           Logger.log (name);
           var sheet = SpreadsheetApp.openById(id); //open the Spam Hunter spreadsheet
           SpreadsheetApp.setActiveSpreadsheet(sheet); //set Spam Hunter as the active spreadsheet
      };
var spammers = GmailApp.getSpamThreads(0, 500);
Logger.log("# of total spam threads: " + GmailApp.getSpamThreads(0, 500).length);

/* ADD SPAMMERS TO THE SPREADSHEET
    function to add the spammer's data to the Spam Hunter Spreadsheet by appending successive rows */
 function addSpammers() {
     SpreadsheetApp.setActiveSheet(sheet.getSheets()[0]);
        for (var k = 0; k < spammers.length; k++) {
        var messages = spammers[k].getMessages();// get messages from spammers thread array
          for (var l = 0; l < messages.length; l++){
              var message = messages[l];
              sheet.appendRow([message.getDate(),message.getFrom(), message.getReplyTo(),message.getTo()]);
          };
       };
       sheet.autoResizeColumn(1);
       sheet.autoResizeColumn(2);
       sheet.autoResizeColumn(3);
       sheet.autoResizeColumn(4);
       removeDuplicates();
  };
/* TABLE DRAWING STARTS HERE  */
function showTable() {
//Set up the charts
  var sheet = SpreadsheetApp.getActiveSheet();
/* This represents ALL the data */
     var range = sheet.getDataRange();
     removeDuplicates();
     var modRange = range.offset(1, 0);
     var values = modRange.getValues();
     Logger.log(values);
     var data = JSON.stringify(values);
     return data;
 };

//Remove Duplicates from the spreadsheet: helpful in both add and view spammers functions
function removeDuplicates() {
  var sheet = SpreadsheetApp.getActiveSheet();
  var data = sheet.getDataRange().getValues();
  var newData = new Array();
  for(i in data){
    var row = data[i];
    var duplicate = false;
    for(j in newData){
      if(row.join() == newData[j].join()){
        duplicate = true;
      }
    }
    if(!duplicate){
      newData.push(row);
    }
  }
  sheet.clearContents();
  sheet.getRange(1, 1, newData.length, newData[0].length).setValues(newData);
};

//DELETE SPAM MESSAGES - Weekly cleanup - needs a for loop function to clear the spam box in first week
function cleanUp() {
  addSpammers();
  archive();
  var spamBeGone = GmailApp.getSpamThreads(0, 500);
  GmailApp.moveThreadsToTrash(spamBeGone);
};

//transcend the 500 message limit in cleaning Spam
function bigClean () {
  for (m=0; m<5000; m+500){
    n=500;
     var spamSet = GmailApp.getSpamThreads([m],[n]);
     GmailApp.moveThreadsToTrash(spamSet);
  };
};

//Set a Weekly Trigger to clean up spam
function weeklyTrigger()  {
  ScriptApp.newTrigger("cleanUp")
   .timeBased()
   .onWeekDay(ScriptApp.WeekDay.SUNDAY)
   .everyWeeks(1)
   .atHour(2)
   .nearMinute(20)
   .create();
 };
//Archives the active spreadsheet and creates a new one for the current week
function archive() {
  var d = new Date();
  var year = d.getFullYear();
  var month = (d.getMonth()+1);
  var day = d.getDate();
  var archName = function () {
    var yr = year.toString();
    var mo = month.toString();
    var dt = day.toString();
    var combo = mo+"/"+dt+"/"+yr;
    return combo;
  }();
  Logger.log(archName);
  var sheet = SpreadsheetApp.getActiveSpreadsheet();
  sheet.renameActiveSheet(archName); //Archived sheets are named by date
  sheet.insertSheet('Current Week', 0); //Make a new sheet for the current week
  sheet.setActiveSheet(sheet.getSheets()[0]);
    // Appends a new row with 4 columns at the top of the first sheet
         sheet.appendRow(["Date", "Spammer", "Reply Address", "Recipient"]);
    // Freezes the first row
         sheet.setFrozenRows(1);
    //format the first row & make the columns pretty
      var range = sheet.getRange("A1:D1");
      range.setBackground("#808080");
      range.setFontSize(14);
      range.setFontFamily("Helvetica");
      range.setFontColor("white");
      sheet.autoResizeColumn(1);
      sheet.autoResizeColumn(2);
      sheet.autoResizeColumn(3);
      sheet.autoResizeColumn(4);
};

/*
//Identify the spammers & recipient
function identify() {
  for (var p = 0; p < spammers.length; p++) {
    var messages = spammers[i].getMessages(); // get messages from spammers thread array
      for (var q = 0; q < messages.length; q++){
          var message = messages[q];
          Logger.log("date: " + message.getDate());
          Logger.log("spammer: " + message.getFrom());
          Logger.log("reply address: " + message.getReplyTo());
          Logger.log("recipient: " + message.getTo());

      };
   };
};
*/
